#!/bin/bash

set -e

r=0

executables=$(rpm -ql gpsd gpsd-clients | grep -E '/s?bin/')
for b in $executables; do
	case "$(basename "$b")" in
		gpsdebuginfo)
			continue;;
		zerk)
			h="-h";;
		*)
			h="--help";;
	esac

	if "$b" "$h" |& grep -qi '^usage *:'; then
		echo "$b OK"
	else
		echo "$b FAILED"
		"$b" "$h" || :
		r=1
	fi
done

exit $r
